﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProjectFreedomServer.Models.EntityFramework;

namespace ProjectFreedomServer
{
    public class Startup
    {
        string _connectionString = null;
        IHostingEnvironment _hostingEnvironment = null;
        //public Startup(IConfiguration configuration)
        //{
        //    Configuration = configuration;
        //}

        public Startup(IHostingEnvironment env)
        {
            _hostingEnvironment = env;

            var builder = new ConfigurationBuilder();

            builder.AddUserSecrets<Startup>();

            Configuration = builder.Build();
        }
        
        public IConfiguration Configuration { get; }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            if (_hostingEnvironment.IsProduction())
            {
                _connectionString = Configuration["ConnectionStringAzureDB"];
            }
            else
            {
                _connectionString = Configuration["ConnectionStringLocalDB"];
            }

            services.AddMvc();
            services.AddDbContext<ProjectFreedomContext>(option =>
                option.UseSqlServer(_connectionString));

            services.AddCors(options =>
            {

                options.AddPolicy("CORS",

                corsPolicyBuilder => corsPolicyBuilder.AllowAnyOrigin()

                // Apply CORS policy for any type of origin

                .AllowAnyMethod()

                // Apply CORS policy for any type of http methods

                .AllowAnyHeader()

                // Apply CORS policy for any headers

                .AllowCredentials());

                // Apply CORS policy for all users

            });
            
            services.AddMvc();
        }
        
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors("CORS");
            app.UseMvc();
        }
    }
}