﻿using Microsoft.EntityFrameworkCore;

namespace ProjectFreedomServer.Models.EntityFramework
{
    public partial class ProjectFreedomContext : DbContext
    {
        public virtual DbSet<Ledamot> Ledamot { get; set; }
        public virtual DbSet<LedamotProcent> LedamotProcent { get; set; }
        public virtual DbSet<Parti> Parti { get; set; }
        public virtual DbSet<PartiProcent> PartiProcent { get; set; }

        //        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //        {
        //            if (!optionsBuilder.IsConfigured)
        //            {
        //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
        //                optionsBuilder.UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;Database=ProjectFreedom;Trusted_Connection=True;");
        //            }
        //        }

        public ProjectFreedomContext(DbContextOptions<ProjectFreedomContext> options) : base(options)
        {
            //Empty
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Ledamot>(entity =>
            {
                entity.HasKey(e => new { e.IntressentId, e.Rm, e.Parti });

                entity.Property(e => e.IntressentId)
                    .HasColumnName("intressent_id")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Rm)
                    .HasColumnName("rm")
                    .HasMaxLength(20);

                entity.Property(e => e.Parti)
                    .HasColumnName("parti")
                    .HasMaxLength(2);

                entity.Property(e => e.Avstår).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.Frånvarande).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.Ja).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.Nej).HasColumnType("decimal(5, 2)");
            });

            modelBuilder.Entity<LedamotProcent>(entity =>
            {
                entity.HasKey(e => new { e.IntressentId, e.Parti });

                entity.Property(e => e.IntressentId)
                    .HasColumnName("intressent_id")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Parti)
                    .HasColumnName("parti")
                    .HasMaxLength(2);

                entity.Property(e => e.Efternamn)
                    .HasColumnName("efternamn")
                    .HasMaxLength(50);

                entity.Property(e => e.Fornamn)
                    .HasColumnName("fornamn")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Frånvarande).HasColumnType("decimal(5, 2)");
            });

            modelBuilder.Entity<Parti>(entity =>
            {
                entity.HasKey(e => new { e.Parti1, e.Rm });

                entity.Property(e => e.Parti1)
                    .HasColumnName("parti")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Rm)
                    .HasColumnName("rm")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Avstår).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.Frånvarande).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.Ja).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.Nej).HasColumnType("decimal(5, 2)");
            });

            modelBuilder.Entity<PartiProcent>(entity =>
            {
                entity.HasKey(e => new { e.Parti, e.Riksdagsår });

                entity.Property(e => e.Parti)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Riksdagsår)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProcentFrånvaro)
                    .HasColumnName("Procent frånvaro")
                    .HasColumnType("decimal(5, 2)");
            });
        }
    }
}
