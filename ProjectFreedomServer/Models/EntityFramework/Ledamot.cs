﻿
namespace ProjectFreedomServer.Models.EntityFramework
{
    public partial class Ledamot
    {
        public string IntressentId { get; set; }
        public string Parti { get; set; }
        public string Rm { get; set; }
        public decimal? Ja { get; set; }
        public decimal? Nej { get; set; }
        public decimal? Avstår { get; set; }
        public decimal? Frånvarande { get; set; }
    }
}