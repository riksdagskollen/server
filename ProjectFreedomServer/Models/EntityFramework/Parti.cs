﻿using System;
using System.Collections.Generic;

namespace ProjectFreedomServer.Models.EntityFramework
{
    public partial class Parti
    {
        public string Parti1 { get; set; }
        public string Rm { get; set; }
        public decimal? Ja { get; set; }
        public decimal? Nej { get; set; }
        public decimal? Avstår { get; set; }
        public decimal? Frånvarande { get; set; }
    }
}
