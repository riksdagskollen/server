﻿using System.Linq;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ProjectFreedomServer.Models.EntityFramework;

namespace ProjectFreedomServer.Controllers
{
    [EnableCors("CORS")]
    [Produces("application/json")]
    [Route("api/Ledamots")]
    public class LedamotsController : Controller
    {
        private readonly ProjectFreedomContext _context;

        public LedamotsController(ProjectFreedomContext context)
        {
            _context = context;
        }

        // GET: api/Ledamots/0100689229625
        [HttpGet("{id}")]
        public IActionResult GetLedamot([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var ledamot = _context.Ledamot.Where(m => m.IntressentId == id);

            if (ledamot == null)
            {
                return NotFound();
            }

            return Ok(ledamot);
        }
    }
}