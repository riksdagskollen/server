﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ProjectFreedomServer.Models.EntityFramework;

namespace ProjectFreedomServer.Controllers
{
    [EnableCors("CORS")]
    [Produces("application/json")]
    [Route("api/PartiProcents")]
    public class PartiProcentsController : Controller
    {
        private readonly ProjectFreedomContext _context;

        public PartiProcentsController(ProjectFreedomContext context)
        {
            _context = context;
        }

        // GET: api/PartiProcents
        [HttpGet]
        public IEnumerable<PartiProcent> GetPartiProcent()
        {
            return _context.PartiProcent;
        }
    }
}