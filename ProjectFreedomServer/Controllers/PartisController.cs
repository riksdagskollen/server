﻿using System.Linq;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ProjectFreedomServer.Models.EntityFramework;

namespace ProjectFreedomServer.Controllers
{
    [EnableCors("CORS")]
    [Produces("application/json")]
    [Route("api/Partis")]
    public class PartisController : Controller
    {
        private readonly ProjectFreedomContext _context;

        public PartisController(ProjectFreedomContext context)
        {
            _context = context;
        }

        // GET: api/Partis/M
        [HttpGet("{id}")]
        public IActionResult GetParti([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parti = _context.Parti.Where(m => m.Parti1 == id);

            if (parti == null)
            {
                return NotFound();
            }

            return Ok(parti);
        }
    }
}