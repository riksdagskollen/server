﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjectFreedomServer.Models.EntityFramework;

namespace ProjectFreedomServer.Controllers
{
    [EnableCors("CORS")]
    [Produces("application/json")]
    [Route("api/LedamotProcents")]
    public class LedamotProcentsController : Controller
    {
        private readonly ProjectFreedomContext _context;

        public LedamotProcentsController(ProjectFreedomContext context)
        {
            _context = context;
        }

        // GET: api/LedamotProcents
        [HttpGet]
        public IEnumerable<LedamotProcent> GetLedamotProcent()
        {
            return _context.LedamotProcent;
        }        
    }
}