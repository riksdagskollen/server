﻿using System;
using System.Collections.Generic;

namespace ProjectFreedomServer.Models.EntityFramework
{
    public partial class PartiProcent
    {
        public string Parti { get; set; }
        public string Riksdagsår { get; set; }
        public decimal? ProcentFrånvaro { get; set; }
    }
}
