﻿using System;
using System.Collections.Generic;

namespace ProjectFreedomServer.Models.EntityFramework
{
    public partial class LedamotProcent
    {
        public string IntressentId { get; set; }
        public string Parti { get; set; }
        public decimal? Frånvarande { get; set; }
        public string Fornamn { get; set; }
        public string Efternamn { get; set; }
    }
}
